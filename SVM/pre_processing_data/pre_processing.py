import pandas as pd
from tqdm import tqdm

def getData():

    dir = '/content/data/'

    df = pd.read_csv(dir + 'labels.csv')

    for index in tqdm(df.index):
        df.loc[index,'path']=df.loc[index,'path'].replace('\\', '/')

    df['path'] = dir + df['path']

    return df
