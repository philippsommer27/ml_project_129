import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import cv2
from scipy.stats import stats
import matplotlib.image as mpimg
from tqdm import tqdm
from PIL import Image

def rgb_split(img):
  blue,green,red = cv2.split(img)
  return blue/255, green/255, red/255

def pca_model(colour):
  model = PCA(n_components=80)
  model_list = list()
  model_list.append(model.fit(colour))
  model_list.append(model_list[0].transform(colour))
  return model_list

def pca(img):
    blue, green, red = rgb_split(img)

    blue_model = pca_model(blue)
    green_model = pca_model(green)
    red_model = pca_model(red)

    img_reduced = (cv2.merge((blue_model[0].inverse_transform(blue_model[1]),
                              green_model[0].inverse_transform(green_model[1]),
                              red_model[0].inverse_transform(red_model[1]))))
    return img_reduced